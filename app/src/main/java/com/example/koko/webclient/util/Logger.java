package com.example.koko.webclient.util;


import android.util.Log;

import java.util.Objects;



/**
 * Created by alexey on 11/23/14.
 */
public class Logger
{
	private String tag;
	private Logger(){

	}


	public Logger(Class clazz){
		super();
		this.tag = clazz.getSimpleName();
	}


	public Logger(Object obj){
		super();
		this.tag = obj.getClass().getSimpleName();
	}


	public void debug(String msg){
		Log.d(tag, msg);
	}

	public void info(String msg){
		Log.i(tag, msg);
	}

}
