package com.example.koko.webclient.http;



/**
 * Created by alexey on 11/22/14.
 */
public class AuthResponse extends ApiResponse<AuthResponse.AuthResult>
{
	public static class AuthResult
	{
		private Integer userId;
		private String sessionId;

		public Integer getUserId()
		{
			return userId;
		}

		public String getSessionId()
		{
			return sessionId;
		}
	}
}
