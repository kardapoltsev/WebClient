package com.example.koko.webclient;


import android.app.Activity;
import android.app.ProgressDialog;
import android.net.http.AndroidHttpClient;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.example.koko.webclient.http.*;
import com.example.koko.webclient.util.AppPreferences;
import com.example.koko.webclient.util.Logger;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.*;



public class MyActivity extends Activity
{
	private Logger logger = new Logger(this);

	private ProgressDialog progressDialog;
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_activity);

		progressDialog = new ProgressDialog(this);
		getCurrentUser();

	}


	private void getCurrentUser(){
		progressDialog.setTitle("Loading");
		progressDialog.show();

		new GetUserTask(new GetUserRequest("current")){
			@Override
			protected void onPostExecute(GetUserResponse response) {
				logger.debug("get user request finished");
				progressDialog.hide();
				if(response.isSuccess()){
					logger.debug("current user name: " + response.getResult().getUser().getName());
				} else if (response.getError().getErrorCode() == ApiResponse.UNAUTHORIZED) {
					auth();
				}
			}
		}.execute();
	}


	private void auth(){
		logger.debug("authorization started");
		progressDialog.show();
		String login = "test";
		String password = "password";

		new AuthTask(new AuthRequest(login, password)) {
			protected void onPostExecute(AuthResponse response) {
				progressDialog.hide();
				logger.debug("auth request finished. Response: " + response);
				if(response.isSuccess()) {
					logger.debug("success auth");
					String sessionId = response.getResult().getSessionId();
					HttpClientHolder.setCookie("wg-s-id", sessionId);
					AppPreferences.getInstance().saveSessionId(sessionId);
					getCurrentUser();
				}
			}
		}.execute();

	}


	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		progressDialog.dismiss();
	}
}
