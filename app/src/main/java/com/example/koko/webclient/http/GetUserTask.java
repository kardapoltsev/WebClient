package com.example.koko.webclient.http;


import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;



/**
 * Created by alexey on 11/23/14.
 */
public class GetUserTask extends BaseHttpTask<GetUserResponse, GetUserRequest> {
	ObjectMapper mapper = new ObjectMapper();

	public GetUserTask(GetUserRequest request) {
		this.request = request;
	}


	@Override
	protected HttpUriRequest createRequest() throws IOException
	{
		return new HttpGet(getHost() + "/api/users/" + request.getUserId());
	}

	@Override
	protected void initResponse()
	{
		this.response = new GetUserResponse();
	}

	@Override
	protected void parseResponse(String result) throws IOException
	{
		GetUserResponse.GetUserResult res = mapper.readValue(result, GetUserResponse.GetUserResult.class);
		response.setResult(res);
	}
}
