package com.example.koko.webclient.http;


/**
 * Created by alexey on 11/22/14.
 */
public class AuthRequest extends ApiRequest {
	private String authId;
	private String password;
	private String authType;


	public AuthRequest(String authId, String password) {
		this.authId = authId;
		this.password = password;
		this.authType = "Direct";
	}


	public String getAuthId()
	{
		return authId;
	}

	public String getPassword()
	{
		return password;
	}

	public String getAuthType()
	{
		return authType;
	}
}
