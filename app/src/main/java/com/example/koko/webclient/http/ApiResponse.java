package com.example.koko.webclient.http;


/**
 * Created by alexey on 11/22/14.
 */
public abstract class ApiResponse<T>
{
	public static int UNKNOWN_ERROR_CODE = -1;
	public static int UNAUTHORIZED = 401;
	public static int OK = 200;

	private ApiError error;
	private T result;


	public void setError(ApiError error)
	{
		this.error = error;
	}

	public ApiError getError()
	{
		return error;
	}


	public void setResult(T result)
	{
		this.result = result;
	}


	public T getResult()
	{
		return result;
	}

	public Boolean isSuccess(){
		return null == error;
	}

	@Override
	public String toString() {
		return "ApiResponse(result: " + result + " error: " + error + ")";
	}

}
