package com.example.koko.webclient.util;


import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.example.koko.webclient.MyApplication;



/**
 * Created by alexey on 11/23/14.
 */
public class AppPreferences
{
	private final static String SESSION_ID_KEY = "sessionId";
	private final static String HOST_KEY = "host";
	private static AppPreferences instance;
	SharedPreferences preferences;
	private AppPreferences(){
		preferences = MyApplication.applicationContext.getSharedPreferences("userconfing", Context.MODE_PRIVATE);
		host = preferences.getString(HOST_KEY, "http://fotik.me");
		sessionId = preferences.getString(SESSION_ID_KEY, null);
		Log.i("APpPref","session id in preferenses: " + sessionId);
	}



	public static synchronized AppPreferences getInstance(){
		if(null == instance){
			instance = new AppPreferences();
		}
		return instance;
	}


	private String host;
	public String getHost(){
		return host;
	}

	private String sessionId;
	public String getSessionId () {return sessionId;}


	public void saveSessionId(String sessionId) {
		Log.i("APpPref","setting session id to : " + sessionId);
		this.sessionId = sessionId;
		preferences.edit().putString(SESSION_ID_KEY, sessionId).commit();
	}

}
