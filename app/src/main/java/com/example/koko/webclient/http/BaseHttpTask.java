package com.example.koko.webclient.http;


import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.util.Log;
import com.example.koko.webclient.util.AppPreferences;
import com.example.koko.webclient.util.Logger;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;

import java.io.*;



/**
 * Created by alexey on 11/23/14.
 */
abstract class BaseHttpTask<Response extends ApiResponse, Request extends ApiRequest> extends AsyncTask<Void, Integer, Response>
{
	protected Logger logger = new Logger(this);
	protected Response response;
	protected Request request;

	protected String getHost() {
		return AppPreferences.getInstance().getHost();
	}


	abstract protected HttpUriRequest createRequest() throws IOException;

	abstract protected void initResponse();

	abstract protected void parseResponse(String result) throws IOException;

	protected void setJsonEntity(HttpEntityEnclosingRequest request, String json) throws UnsupportedEncodingException
	{
		StringEntity se = new StringEntity(json);
		request.setEntity(se);
		request.setHeader("Content-type", "application/json");
	}


	protected Response doInBackground(Void... voids)
	{
		try {
			initResponse();
			HttpUriRequest request = createRequest();
			logger.debug("executing request: " + request);
			HttpResponse httpResponse;
			httpResponse = HttpClientHolder.getHttpClient().execute(request, HttpClientHolder.getHttpContext());

			HttpEntity entity = httpResponse.getEntity();
			Integer statusCode = httpResponse.getStatusLine().getStatusCode();
			//TODO: remove 302
			if ((statusCode == ApiResponse.OK || statusCode == 302) && entity != null) {
				InputStream instream = entity.getContent();
				String result = convertStreamToString(instream);
				Log.i(getClass().getSimpleName(), "got result: " + result);
				instream.close();

				parseResponse(result);
				logger.debug("got server response: " + response);
				return response;
			} else {
				response.setError(new ApiError(statusCode, ""));
				logger.debug("got server response: " + response);
				return response;
			}
		} catch (IOException e) {
			Log.e(getClass().getSimpleName(), "error during authorization", e);
			response.setError(new ApiError(ApiResponse.UNKNOWN_ERROR_CODE, e.getMessage()));
			return response;
		}
	}


	protected String convertStreamToString(InputStream is)
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

}
