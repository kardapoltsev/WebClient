package com.example.koko.webclient.http;


import android.net.http.AndroidHttpClient;
import android.util.Log;
import com.example.koko.webclient.util.AppPreferences;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;



/**
 * Created by alexey on 11/23/14.
 */
public class HttpClientHolder
{
	private static HttpClientHolder instance;
	private static HttpClient httpClient;
	private static CookieStore cookieStore;
	private static HttpContext httpContext;

	private HttpClientHolder(){
		httpClient = AndroidHttpClient.newInstance("user-agent");
    cookieStore = new BasicCookieStore();

    httpContext = new BasicHttpContext();
    httpContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
		String sessionId = AppPreferences.getInstance().getSessionId();
		if(null != sessionId) {
			Log.i("Http holder", "setting session id to " + sessionId);
			setCookie("wg-s-id", sessionId);
		}
	}

	public synchronized static HttpClient getHttpClient(){
		if(null == instance){
			instance = new HttpClientHolder();
		}
		return httpClient;
	}

	public static synchronized void setCookie(String name, String value){
		cookieStore.addCookie(new BasicClientCookie(name, value));
	}

	public static synchronized HttpContext getHttpContext(){
		return httpContext;
	}

}
