package com.example.koko.webclient.http;


/**
 * Created by alexey on 11/23/14.
 */
public class GetUserRequest extends ApiRequest
{
	private String userId;

	private GetUserRequest(){

	}

	public GetUserRequest(String userId) {
		super();
		this.userId = userId;
	}

	public String getUserId() {
		return userId;
	}
}
