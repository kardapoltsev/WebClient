package com.example.koko.webclient.http;


/**
 * Created by alexey on 11/22/14.
 */
public class ApiError {

	public ApiError(Integer code, String message) {
		super();
		this.errorCode = code;
		this.errorMessage = message;
	}

	private Integer errorCode;
	private String errorMessage;

	public Integer getErrorCode()
	{
		return errorCode;
	}


	@Override
	public String toString(){
		return "ApiError(code: " + errorCode + " message: " + errorMessage + ")";
	}
}
