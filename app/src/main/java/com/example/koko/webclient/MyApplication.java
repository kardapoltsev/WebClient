package com.example.koko.webclient;


import android.app.Application;
import android.content.Context;



/**
 * Created by alexey on 11/23/14.
 */
public class MyApplication extends Application
{
	public static volatile Context applicationContext = null;

	@Override
	public void onCreate()
	{
		super.onCreate();
		applicationContext = getApplicationContext();
	}

}
