package com.example.koko.webclient.http;


/**
 * Created by alexey on 11/23/14.
 */
public class GetUserResponse extends ApiResponse<GetUserResponse.GetUserResult>
{
	public static class GetUserResult {
		private UserInfo user;

		public UserInfo getUser(){
			return user;
		}
	}

	public static class UserInfo {
		private String id;
	 	private String name;
		private Integer avatarId;
		private String registrationTime;

		public String getId()
		{
			return id;
		}

		public Integer getAvatarId()
		{
			return avatarId;
		}

		public String getRegistrationTime()
		{
			return registrationTime;
		}

		public String getName()
		{
			return name;

		}
	}
}
