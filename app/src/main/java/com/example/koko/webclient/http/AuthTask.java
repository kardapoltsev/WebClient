package com.example.koko.webclient.http;


import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;



/**
 * Created by alexey on 11/23/14.
 */
public class AuthTask extends BaseHttpTask<AuthResponse, AuthRequest> {
	ObjectMapper mapper = new ObjectMapper();

	public AuthTask(AuthRequest auth) {
		request = auth;
	}


	@Override
	protected HttpUriRequest createRequest() throws IOException
	{
		HttpPost httpRequest = new HttpPost(getHost() + "/api/auth");
		String json = mapper.writeValueAsString(request);
		setJsonEntity(httpRequest, json);
		return httpRequest;
	}

	@Override
	protected void initResponse()
	{
		this.response = new AuthResponse();
	}

	@Override
	protected void parseResponse(String result) throws IOException
	{
		AuthResponse.AuthResult authResult = mapper.readValue(result, AuthResponse.AuthResult.class);
		response.setResult(authResult);
	}
}
